package com.appicmind.testcase.data.remote.mapper

import com.appicmind.testcase.data.remote.entity.RemoteCatalogItemEntity
import org.junit.Test

/**
 * @author marcocoenradie
 * @since 04-09-2019
 */
class RemoteCatalogItemEntityMapperTest {

    private val mapper = RemoteCatalogItemEntityMapper()

    private val testEntity = RemoteCatalogItemEntity(
        "the id",
        "a base64 encoded image",
        "some test",
        0.5)

    @Test
    fun `transform empty list returns empty list`() {
        assert(mapper.transform(emptyList()).isEmpty())
    }

    @Test
    fun `transform single item returns object with correct values`() {
        val values = mapper.transform(listOf(testEntity))
        assert(values.isNotEmpty())

        val value = values.first()
        assert(value.id == testEntity._id)
        assert(value.image == testEntity.img)
        assert(value.text == testEntity.text)
        assert(value.confidence == testEntity.confidence)
    }

    @Test
    fun `transform invalid item returns empty list`() {
        assert(mapper.transform(listOf(testEntity.copy(_id = null))).isEmpty())
        assert(mapper.transform(listOf(testEntity.copy(img = null))).isEmpty())
        assert(mapper.transform(listOf(testEntity.copy(text = null))).isEmpty())
        assert(mapper.transform(listOf(testEntity.copy(confidence = null))).isEmpty())
    }

}