package com.appicmind.testcase.domain

/**
 * @author marcocoenradie
 * @since 03-09-2019
 */
class RepositoryCallback<T>(
    private val status: LoadingStatus,
    val response: T? = null,
    val message: String? = null) {

    enum class LoadingStatus {
        REFRESHING,
        LOADING_MORE_RECENT,
        LOADING_LESS_RECENT,
        SUCCESS_REFRESHING,
        DELETING,
        SUCCESS_LOADING_MORE_RECENT,
        SUCCESS_LOADING_LESS_RECENT,
        SUCCESS_DELETING,
        ERROR_REFRESHING,
        ERROR_LOADING_MORE_RECENT,
        ERROR_LOADING_LESS_RECENT,
        ERROR_DELETING
    }

    fun isRefreshing() = status == LoadingStatus.REFRESHING
    fun isLoadingMoreRecent() = status == LoadingStatus.LOADING_MORE_RECENT
    fun isLoadingLessRecent() = status == LoadingStatus.LOADING_LESS_RECENT
    fun isDeleting() = status == LoadingStatus.DELETING
    fun isSuccessRefreshing() = status == LoadingStatus.SUCCESS_REFRESHING
    fun isSuccessLoadingMoreRecent() = status == LoadingStatus.SUCCESS_REFRESHING
    fun isSuccessLoadingLessRecent() = status == LoadingStatus.SUCCESS_REFRESHING
    fun isSuccessDeleting() = status == LoadingStatus.SUCCESS_DELETING
    fun isErrorRefreshing() = status == LoadingStatus.ERROR_REFRESHING
    fun isErrorLoadingMoreRecent() = status == LoadingStatus.ERROR_LOADING_MORE_RECENT
    fun isErrorLoadingLessRecent() = status == LoadingStatus.ERROR_LOADING_LESS_RECENT
    fun isErrorDeleting() = status == LoadingStatus.ERROR_DELETING

    fun isLoading() = isRefreshing() || isLoadingMoreRecent()
            || isLoadingLessRecent() || isDeleting()
    fun isSuccess() = isSuccessRefreshing() || isSuccessLoadingMoreRecent()
            || isSuccessLoadingLessRecent() || isSuccessDeleting()
    fun isError() = isErrorRefreshing() || isErrorLoadingMoreRecent()
            || isErrorLoadingLessRecent() || isErrorDeleting()

}