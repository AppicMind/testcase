package com.appicmind.testcase.domain

/**
 * @author marcocoenradie
 * @since 03-09-2019
 */
data class CatalogItem(
    val id: String,
    val base64Image: String,
    val text : String,
    val confidence: Double
)