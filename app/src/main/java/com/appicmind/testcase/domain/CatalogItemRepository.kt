package com.appicmind.testcase.domain

/**
 * @author marcocoenradie
 * @since 03-09-2019
 */
interface CatalogItemRepository {
    fun setCatalogItemsChangedListener(changedListener: CatalogItemsChangedListener?)
    fun refreshCatalogItems()
    fun fetchMoreRecentCatalogItems()
    fun fetchLessRecentCatalogItems()

    interface CatalogItemsChangedListener {
        fun onCatalogItemsChanged(callback: RepositoryCallback<List<CatalogItem>>)
    }
}