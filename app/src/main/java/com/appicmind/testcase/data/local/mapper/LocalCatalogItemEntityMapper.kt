package com.appicmind.testcase.data.local.mapper

import com.appicmind.testcase.data.local.entity.LocalCatalogItemEntity
import com.appicmind.testcase.domain.CatalogItem

/**
 * @author marcocoenradie
 * @since 03-09-2019
 */
class LocalCatalogItemEntityMapper {

    fun transform(catalogItemEntities: List<LocalCatalogItemEntity>): List<CatalogItem> = catalogItemEntities
        .map(::transform)

    private fun transform(catalogItemEntity: LocalCatalogItemEntity): CatalogItem = CatalogItem(
        catalogItemEntity.id,
        catalogItemEntity.image,
        catalogItemEntity.text,
        catalogItemEntity.confidence
    )

}