package com.appicmind.testcase.data.repository

import androidx.lifecycle.MediatorLiveData
import com.appicmind.testcase.data.local.database.CatalogItemDao
import com.appicmind.testcase.data.local.mapper.LocalCatalogItemEntityMapper
import com.appicmind.testcase.data.remote.api.Api
import com.appicmind.testcase.data.remote.entity.RemoteCatalogItemEntity
import com.appicmind.testcase.data.remote.mapper.RemoteCatalogItemEntityMapper
import com.appicmind.testcase.domain.CatalogItem
import com.appicmind.testcase.domain.CatalogItemRepository
import com.appicmind.testcase.domain.RepositoryCallback
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author marcocoenradie
 * @since 03-09-2019
 */
class CatalogItemDataRepository constructor(
    private val catalogItemDao: CatalogItemDao,
    private val api: Api,
    private val localCatalogItemEntityMapper: LocalCatalogItemEntityMapper,
    private val remoteCatalogItemEntityMapper: RemoteCatalogItemEntityMapper
) : CatalogItemRepository {

    private val catalogItems = MediatorLiveData<RepositoryCallback<List<CatalogItem>>>()
    private var catalogItemsChangedListener: CatalogItemRepository.CatalogItemsChangedListener? = null

    init {
        catalogItems.observeForever { callback ->
            catalogItemsChangedListener?.onCatalogItemsChanged(callback)
        }
    }

    override fun setCatalogItemsChangedListener(changedListener: CatalogItemRepository.CatalogItemsChangedListener?) {
        this.catalogItemsChangedListener = changedListener
        catalogItems.value?.let {
            catalogItemsChangedListener?.onCatalogItemsChanged(it)
        }
    }

    override fun refreshCatalogItems() {
        val localSourceAdded = catalogItems.value != null
        if (!localSourceAdded) {
            catalogItems.addSource(catalogItemDao.getAll()) { entities ->
                val shouldFetchRemote = catalogItems.value == null

                val loadingStatus = when {
                    catalogItems.value?.isDeleting() == true -> RepositoryCallback.LoadingStatus.SUCCESS_DELETING
                    catalogItems.value?.isLoadingMoreRecent() == true -> RepositoryCallback.LoadingStatus.SUCCESS_LOADING_MORE_RECENT
                    catalogItems.value?.isLoadingLessRecent() == true -> RepositoryCallback.LoadingStatus.SUCCESS_LOADING_LESS_RECENT
                    else -> RepositoryCallback.LoadingStatus.SUCCESS_REFRESHING
                }

                catalogItems.value = RepositoryCallback(
                    loadingStatus,
                    localCatalogItemEntityMapper.transform(entities)
                )

                if (shouldFetchRemote) {
                    fetchRemoteCatalogItems(clearLocalItems = true)
                }
            }
        } else {
            fetchRemoteCatalogItems(clearLocalItems = true)
        }
    }

    override fun fetchMoreRecentCatalogItems() {
        val sinceId = catalogItems.value?.response?.firstOrNull()?.id
        fetchRemoteCatalogItems(sinceId = sinceId)
    }

    override fun fetchLessRecentCatalogItems() {
        val maxId = catalogItems.value?.response?.lastOrNull()?.id
        fetchRemoteCatalogItems(maxId = maxId)
    }

    private fun fetchRemoteCatalogItems(sinceId: String? = null, maxId: String? = null, clearLocalItems: Boolean = false) {
        if (catalogItems.value?.isLoading() == true) {
            // a request is already loading
            return
        }

        val loadingStatus = when {
            sinceId != null -> RepositoryCallback.LoadingStatus.LOADING_MORE_RECENT
            maxId != null -> RepositoryCallback.LoadingStatus.LOADING_LESS_RECENT
            else -> RepositoryCallback.LoadingStatus.REFRESHING
        }
        catalogItems.value = RepositoryCallback(
            loadingStatus,
            catalogItems.value?.response
        )

        api.getCatalogItems(sinceId, maxId).enqueue(object: Callback<List<RemoteCatalogItemEntity>> {
            override fun onFailure(call: Call<List<RemoteCatalogItemEntity>>, t: Throwable) {
                val loadingStatus = when {
                    catalogItems.value?.isLoadingMoreRecent() == true -> RepositoryCallback.LoadingStatus.ERROR_LOADING_MORE_RECENT
                    catalogItems.value?.isLoadingLessRecent() == true -> RepositoryCallback.LoadingStatus.ERROR_LOADING_LESS_RECENT
                    else -> RepositoryCallback.LoadingStatus.ERROR_REFRESHING
                }
                catalogItems.postValue(
                    RepositoryCallback(
                        loadingStatus,
                        catalogItems.value?.response,
                        t.message
                    )
                )
            }

            override fun onResponse(
                call: Call<List<RemoteCatalogItemEntity>>,
                response: Response<List<RemoteCatalogItemEntity>>
            ) {
                val statusCode = response.code()
                val body = response.body()
                if (statusCode < 200 || statusCode >= 300 || body == null) {
                    onFailure(call, Throwable("Server error: ${response.errorBody()?.string()}"))
                    return
                }

                if (clearLocalItems) {
                    catalogItemDao.deleteAll()
                }

                val newItems = remoteCatalogItemEntityMapper.transform(body)
                if (!clearLocalItems && newItems.isEmpty()) {
                    // if there are no new items, still trigger a refresh on the LiveDate
                    val loadingStatus = when {
                        catalogItems.value?.isLoadingMoreRecent() == true -> RepositoryCallback.LoadingStatus.ERROR_LOADING_MORE_RECENT
                        catalogItems.value?.isLoadingLessRecent() == true -> RepositoryCallback.LoadingStatus.ERROR_LOADING_LESS_RECENT
                        else -> RepositoryCallback.LoadingStatus.ERROR_REFRESHING
                    }
                    catalogItems.postValue(
                        RepositoryCallback(
                            loadingStatus,
                            catalogItems.value?.response
                        )
                    )
                } else {
                    catalogItemDao.insertAll(newItems)
                }
            }

        })
    }

    fun deleteCatalogItem(catalogItem: CatalogItem) {
        catalogItems.value = RepositoryCallback(
            RepositoryCallback.LoadingStatus.DELETING,
            catalogItems.value?.response
        )

        api.deleteCatalogItem(catalogItem.id).enqueue(object: Callback<Void> {
            override fun onFailure(call: Call<Void>, t: Throwable) {
                catalogItems.postValue(
                    RepositoryCallback(
                        RepositoryCallback.LoadingStatus.ERROR_DELETING,
                        catalogItems.value?.response,
                        t.message
                    )
                )
            }

            override fun onResponse(
                call: Call<Void>,
                response: Response<Void>
            ) {
                val statusCode = response.code()
                if (statusCode < 200 || statusCode >= 300) {
                    onFailure(call, Throwable("Server error: ${response.errorBody()?.string()}"))
                    return
                }
                catalogItemDao.delete(catalogItem.id)
            }

        })
    }

}