package com.appicmind.testcase.data.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.appicmind.testcase.data.local.entity.LocalCatalogItemEntity

/**
 * @author marcocoenradie
 * @since 03-09-2019
 */
@Database(entities = [LocalCatalogItemEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun catalogItemDao(): CatalogItemDao

}