package com.appicmind.testcase.data.remote.api

import com.appicmind.testcase.data.remote.entity.RemoteCatalogItemEntity
import retrofit2.Call
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * @author marcocoenradie
 * @since 03-09-2019
 */
interface Api {

    @GET("items")
    fun getCatalogItems(@Query("since_id") sinceId: String?,
                        @Query("max_id") maxId: String?): Call<List<RemoteCatalogItemEntity>>

    @DELETE("item/{itemId}")
    fun deleteCatalogItem(@Path("itemId") itemId: String): Call<Void>
}