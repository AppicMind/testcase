package com.appicmind.testcase.data.remote.entity

/**
 * @author marcocoenradie
 * @since 03-09-2019
 */
data class RemoteCatalogItemEntity(
    val _id: String?,
    val img: String?,
    val text : String?,
    val confidence: Double?
)