package com.appicmind.testcase.data.local.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.appicmind.testcase.data.local.entity.LocalCatalogItemEntity

/**
 * @author marcocoenradie
 * @since 03-09-2019
 */
@Dao
interface CatalogItemDao {

    @Query("SELECT * FROM catalog_item ORDER BY id DESC")
    fun getAll(): LiveData<List<LocalCatalogItemEntity>>

    @Insert(onConflict = REPLACE)
    fun insertAll(catalogItems: List<LocalCatalogItemEntity>)

    @Insert(onConflict = REPLACE)
    fun insert(catalogItem: LocalCatalogItemEntity)

    @Query("DELETE FROM catalog_item WHERE id = :itemId")
    fun delete(itemId: String)

    @Query("DELETE FROM catalog_item")
    fun deleteAll()
}