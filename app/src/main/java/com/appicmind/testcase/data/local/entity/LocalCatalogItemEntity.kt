package com.appicmind.testcase.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @author marcocoenradie
 * @since 03-09-2019
 */
@Entity(tableName = "catalog_item")
data class LocalCatalogItemEntity (
    @PrimaryKey val id: String,
    val image: String,
    val text : String,
    val confidence: Double
)