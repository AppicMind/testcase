package com.appicmind.testcase.data.remote.api

import com.appicmind.testcase.BuildConfig
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.CertificatePinner
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

/**
 * @author marcocoenradie
 * @since 03-09-2019
 */
class ApiProvider {

    companion object {
        private const val API_HOST_NAME = "marlove.net"
        private const val API_BASE_URL = "https://$API_HOST_NAME/e/mock/v1/"
        private const val NETWORK_TIMEOUT = 30L
    }

    private fun provideGson() = GsonBuilder().create()

    private fun provideOkHttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder().apply {
            certificatePinner(provideCertificatePinner())
            connectTimeout(NETWORK_TIMEOUT, TimeUnit.SECONDS)
            readTimeout(NETWORK_TIMEOUT, TimeUnit.SECONDS)
            writeTimeout(NETWORK_TIMEOUT, TimeUnit.SECONDS)
            addInterceptor(object : Interceptor {
                override fun intercept(chain: Interceptor.Chain): Response {
                    val builder = chain.request().newBuilder()
                    builder.addHeader("Authorization", "001e5fc6ba8e9f2d0c881c8b6decd63c")
                    return chain.proceed(builder.build())
                }
            })
            if (BuildConfig.DEBUG) {
                interceptors().add(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                })
            }
        }
        return builder.build()
    }

    private fun provideCertificatePinner() = CertificatePinner.Builder()
        .add(API_HOST_NAME, "sha256/rCCCPxtKvFVDrKOPDSfirp4bQOYw4mIVKn8fZxgQcs4=")
        .add(API_HOST_NAME, "sha256/klO23nT2ehFDXCfx3eHTDRESMz3asj1muO+4aIdjiuY=")
        .add(API_HOST_NAME, "sha256/grX4Ta9HpZx6tSHkmCrvpApTQGo67CYDnvprLg5yRME=")
        .build()

    private fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient) = Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .callbackExecutor(Executors.newSingleThreadExecutor())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient)
            .build()

    fun provideApi(): Api = provideRetrofit(
            provideGson(),
            provideOkHttpClient()
        ).create(Api::class.java)

}