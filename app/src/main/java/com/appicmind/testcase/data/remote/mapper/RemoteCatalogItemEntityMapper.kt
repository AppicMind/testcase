package com.appicmind.testcase.data.remote.mapper

import com.appicmind.testcase.data.local.entity.LocalCatalogItemEntity
import com.appicmind.testcase.data.remote.entity.RemoteCatalogItemEntity

/**
 * @author marcocoenradie
 * @since 03-09-2019
 */
class RemoteCatalogItemEntityMapper {

    fun transform(catalogItemEntities: List<RemoteCatalogItemEntity>): List<LocalCatalogItemEntity> = catalogItemEntities
        .mapNotNull(::safeTransform)

    private fun safeTransform(catalogItemEntity: RemoteCatalogItemEntity) = try {
        transform(catalogItemEntity)
    } catch (e: Exception) {
        null
    }

    private fun transform(catalogItemEntity: RemoteCatalogItemEntity) = LocalCatalogItemEntity(
        requireNotNull(catalogItemEntity._id) { "_id" },
        requireNotNull(catalogItemEntity.img) { "img" },
        requireNotNull(catalogItemEntity.text) { "text" },
        requireNotNull(catalogItemEntity.confidence) { "confidence" }
    )

}