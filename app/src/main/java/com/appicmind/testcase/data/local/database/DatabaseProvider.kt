package com.appicmind.testcase.data.local.database

import com.appicmind.testcase.presentation.App

/**
 * @author marcocoenradie
 * @since 03-09-2019
 */
class DatabaseProvider {

    fun provideDatabase() = App.instance.database

}