package com.appicmind.testcase.data.repository

import com.appicmind.testcase.data.local.database.DatabaseProvider
import com.appicmind.testcase.data.local.mapper.LocalCatalogItemEntityMapper
import com.appicmind.testcase.data.remote.api.ApiProvider
import com.appicmind.testcase.data.remote.mapper.RemoteCatalogItemEntityMapper

/**
 * @author marcocoenradie
 * @since 03-09-2019
 */
class CatalogItemRepositoryProvider {

    private val databaseProvider = DatabaseProvider()
    private val apiProvider = ApiProvider()
    private val remoteCatalogItemEntityMapper = RemoteCatalogItemEntityMapper()
    private val localCatalogItemEntityMapper = LocalCatalogItemEntityMapper()

    fun provideCatalogItemRepository() = CatalogItemDataRepository(
        databaseProvider.provideDatabase().catalogItemDao(),
        apiProvider.provideApi(),
        localCatalogItemEntityMapper,
        remoteCatalogItemEntityMapper
    )

}