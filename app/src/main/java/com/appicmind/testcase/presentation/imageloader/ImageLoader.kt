package com.appicmind.testcase.presentation.imageloader

import android.graphics.BitmapFactory
import android.util.Base64
import android.widget.ImageView


/**
 * @author marcocoenradie
 * @since 03-09-2019
 */
class ImageLoader {

    fun loadBase64Image(encodedImage: String, imageView: ImageView) {
        //TODO do this on a background thread and cache; maybe use Glide or other lib
        val decodedString = Base64.decode(encodedImage, Base64.DEFAULT)
        val bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        imageView.setImageBitmap(bitmap)
    }

}