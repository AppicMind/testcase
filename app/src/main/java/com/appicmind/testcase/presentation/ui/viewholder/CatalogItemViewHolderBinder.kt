package com.appicmind.testcase.presentation.ui.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.appicmind.testcase.R
import com.appicmind.testcase.domain.CatalogItem
import com.appicmind.testcase.presentation.imageloader.ImageLoader
import kotlinx.android.synthetic.main.list_catalog_item.view.*

/**
 * @author marcocoenradie
 * @since 04-09-2019
 */
class CatalogItemViewHolderBinder {

    private val imageLoader = ImageLoader()
    var onLongClickListener : OnCatalogItemLongClickListener? = null

    fun createViewHolder(parent: ViewGroup) = CatalogItemViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_catalog_item, parent, false)
        )

    fun bindViewHolder(holder: CatalogItemViewHolder, item: CatalogItem) {
        imageLoader.loadBase64Image(item.base64Image, holder.imageView)
        holder.idView.text = item.id
        holder.textView.text = item.text
        holder.confidenceView.text = item.confidence.toString()
        holder.itemView.setOnLongClickListener {
            onLongClickListener?.onCatalogItemLongClick(item) == true
        }
    }

    inner class CatalogItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageView: ImageView = view.image
        val idView: TextView = view.item_id
        val textView: TextView = view.item_text
        val confidenceView: TextView = view.item_confidence
    }

    interface OnCatalogItemLongClickListener {
        fun onCatalogItemLongClick(item: CatalogItem): Boolean
    }
}