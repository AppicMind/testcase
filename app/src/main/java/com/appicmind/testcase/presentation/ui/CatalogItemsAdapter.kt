package com.appicmind.testcase.presentation.ui

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.appicmind.testcase.domain.CatalogItem
import com.appicmind.testcase.presentation.ui.viewholder.CatalogItemViewHolderBinder
import com.appicmind.testcase.presentation.ui.viewholder.EmptyViewHolderBinder
import com.appicmind.testcase.presentation.ui.viewholder.LoadMoreItemViewHolderBinder
import com.appicmind.testcase.presentation.ui.viewholder.ViewType

/**
 * @author marcocoenradie
 * @since 03-09-2019
 */
class CatalogItemsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val catalogItemViewHolderBinder =
        CatalogItemViewHolderBinder()
    private val loadMoreItemViewHolderBinder =
        LoadMoreItemViewHolderBinder()
    private val emptyViewHolderBinder =
        EmptyViewHolderBinder()

    var catalogItems: List<CatalogItem> = emptyList()
    var shouldShowLoadMoreIndicator = false
    var shouldShowEmptyView = false

    init {
        setHasStableIds(true)
    }

    override fun getItemId(position: Int): Long = with(getItemViewType(position)) {
        when (this) {
            ViewType.CATALOG_ITEM.value -> catalogItems[position].id.hashCode().toLong()
            else -> this.toLong()
        }
    }

    override fun getItemViewType(position: Int): Int = when {
            catalogItems.isEmpty() && shouldShowEmptyView -> ViewType.EMPTY_VIEW.value
            position >= 0 && position < catalogItems.size -> ViewType.CATALOG_ITEM.value
            else -> ViewType.LOAD_MORE.value
        }

    override fun getItemCount(): Int = when {
            catalogItems.isEmpty() && shouldShowEmptyView -> 1
            shouldShowLoadMoreIndicator -> catalogItems.size + 1
            else -> catalogItems.size
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = when(viewType) {
            ViewType.CATALOG_ITEM.value -> catalogItemViewHolderBinder.createViewHolder(parent)
            ViewType.LOAD_MORE.value -> loadMoreItemViewHolderBinder.createViewHolder(parent)
            ViewType.EMPTY_VIEW.value -> emptyViewHolderBinder.createViewHolder(parent)
            else -> throw IllegalArgumentException("Unknown viewType $viewType")
        }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        when(getItemViewType(position)) {
            ViewType.CATALOG_ITEM.value -> {
                val holder = viewHolder as CatalogItemViewHolderBinder.CatalogItemViewHolder
                catalogItemViewHolderBinder.bindViewHolder(holder, catalogItems[position])
            }
            ViewType.LOAD_MORE.value -> {
                loadMoreItemViewHolderBinder.bindViewHolder(viewHolder, position)
            }
        }
    }

    fun setLoadMoreHandler(loadMoreHandler: LoadMoreItemViewHolderBinder.LoadMoreHandler) {
        loadMoreItemViewHolderBinder.loadMoreHandler = loadMoreHandler
    }

    fun setOnCatalogItemLongClickListener(onLongClickListener: CatalogItemViewHolderBinder.OnCatalogItemLongClickListener?) {
        catalogItemViewHolderBinder.onLongClickListener = onLongClickListener
    }
}