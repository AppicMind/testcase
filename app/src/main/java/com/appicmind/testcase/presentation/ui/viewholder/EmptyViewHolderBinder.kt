package com.appicmind.testcase.presentation.ui.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.appicmind.testcase.R

/**
 * @author marcocoenradie
 * @since 04-09-2019
 */
class EmptyViewHolderBinder {

    fun createViewHolder(parent: ViewGroup) =
        EmptyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_empty_item, parent, false)
        )

    inner class EmptyViewHolder(view: View) : RecyclerView.ViewHolder(view)
}