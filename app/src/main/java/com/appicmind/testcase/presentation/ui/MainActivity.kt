package com.appicmind.testcase.presentation.ui

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.appicmind.testcase.domain.RepositoryCallback
import com.appicmind.testcase.domain.CatalogItem
import com.appicmind.testcase.domain.CatalogItemRepository
import com.appicmind.testcase.presentation.ui.viewholder.CatalogItemViewHolderBinder
import com.appicmind.testcase.presentation.ui.viewholder.LoadMoreItemViewHolderBinder
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*


/**
 * An activity representing a list of [CatalogItem]s.
 */
class MainActivity : AppCompatActivity(),
    CatalogItemRepository.CatalogItemsChangedListener,
    LoadMoreItemViewHolderBinder.LoadMoreHandler,
    CatalogItemViewHolderBinder.OnCatalogItemLongClickListener {

    private lateinit var viewModel: MainViewModel
    private val adapter = CatalogItemsAdapter().apply {
        setLoadMoreHandler(this@MainActivity)
        setOnCatalogItemLongClickListener(this@MainActivity)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.appicmind.testcase.R.layout.activity_main)

        setupToolbar()
        setupSwipeRefreshLayout()

        setupViewModel()
        if (savedInstanceState == null) {
            viewModel.refreshCatalogItems()
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        toolbar.title = title
    }

    private fun setupSwipeRefreshLayout() {
        swipe_refresh_item_list.setOnRefreshListener {
            viewModel.fetchMoreRecentCatalogItems()
        }
    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(this)[MainViewModel::class.java].apply {
            observeCatalogItems(this@MainActivity, this@MainActivity)
        }
    }

    override fun loadMore() {
        viewModel.fetchLessRecentCatalogItems()
    }

    override fun onCatalogItemLongClick(item: CatalogItem): Boolean {
        AlertDialog.Builder(this)
            .setTitle("Are you sure?")
            .setMessage("Delete ${item.id}?")
            .setPositiveButton(android.R.string.yes) { dialog, which ->
                viewModel.deleteCatalogItem(item)
            }
            .setNegativeButton(android.R.string.no, null)
            .show()
        return true
    }

    override fun onCatalogItemsChanged(callback: RepositoryCallback<List<CatalogItem>>) {
        swipe_refresh_item_list.isRefreshing = callback.isLoadingMoreRecent()
        callback.message?.let {
            Snackbar.make(swipe_refresh_item_list, it, Snackbar.LENGTH_INDEFINITE)
                .setAction(android.R.string.ok) {}
                .show()
        }
        item_list.post {
            adapter.catalogItems = callback.response.orEmpty()
            adapter.shouldShowEmptyView =
                callback.response.isNullOrEmpty() && !callback.isLoading()
            adapter.shouldShowLoadMoreIndicator =
                !callback.response.isNullOrEmpty()  && !callback.isErrorLoadingLessRecent()
            adapter.notifyDataSetChanged()

            if (item_list.adapter == null) {
                item_list.adapter = adapter
            }
        }
    }

}
