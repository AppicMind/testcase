package com.appicmind.testcase.presentation

import android.app.Application
import androidx.room.Room
import com.appicmind.testcase.data.local.database.AppDatabase

/**
 * @author marcocoenradie
 * @since 03-09-2019
 */
class App : Application() {

    val database: AppDatabase by lazy {
        Room.databaseBuilder(
            this,
            AppDatabase::class.java, "database-name"
        ).build()
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object {
        lateinit var instance: App
    }
}