package com.appicmind.testcase.presentation.ui.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.appicmind.testcase.R

/**
 * @author marcocoenradie
 * @since 04-09-2019
 */
class LoadMoreItemViewHolderBinder {

    var loadMoreHandler: LoadMoreHandler? = null

    fun createViewHolder(parent: ViewGroup) = LoadingIndicatorViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_loading_item, parent, false)
        )

    fun bindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        loadMoreHandler?.loadMore()
    }

    inner class LoadingIndicatorViewHolder(view: View) : RecyclerView.ViewHolder(view)

    interface LoadMoreHandler {
        fun loadMore()
    }
}