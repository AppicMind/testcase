package com.appicmind.testcase.presentation.ui

import androidx.lifecycle.*
import com.appicmind.testcase.data.repository.CatalogItemRepositoryProvider
import com.appicmind.testcase.domain.CatalogItem
import com.appicmind.testcase.domain.CatalogItemRepository

/**
 * @author marcocoenradie
 * @since 03-09-2019
 */
class MainViewModel : ViewModel(), LifecycleObserver {

    private val catalogItemRepository = CatalogItemRepositoryProvider().provideCatalogItemRepository()
    private var catalogItemsChangedListener: CatalogItemRepository.CatalogItemsChangedListener? = null

    private val lifecycleObserver = object : LifecycleObserver {
        @OnLifecycleEvent(Lifecycle.Event.ON_START)
        fun onStart() {
            catalogItemRepository.setCatalogItemsChangedListener(catalogItemsChangedListener)
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
        fun onStop() {
            catalogItemRepository.setCatalogItemsChangedListener(null)
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        fun oDestroy() {
            catalogItemsChangedListener = null
        }
    }

    fun observeCatalogItems(lifecycleOwner: LifecycleOwner, changedListener: CatalogItemRepository.CatalogItemsChangedListener) {
        catalogItemsChangedListener = changedListener
        lifecycleOwner.lifecycle.addObserver(lifecycleObserver)
    }

    fun refreshCatalogItems() {
        catalogItemRepository.refreshCatalogItems()
    }

    fun fetchMoreRecentCatalogItems() {
        catalogItemRepository.fetchMoreRecentCatalogItems()
    }

    fun fetchLessRecentCatalogItems() {
        catalogItemRepository.fetchLessRecentCatalogItems()
    }

    fun deleteCatalogItem(catalogItem: CatalogItem) {
        catalogItemRepository.deleteCatalogItem(catalogItem)
    }

}