package com.appicmind.testcase.presentation.ui.viewholder

/**
 * @author marcocoenradie
 * @since 30-09-2019
 */
enum class ViewType(val value: Int) {
    EMPTY_VIEW(0),
    LOAD_MORE(1),
    CATALOG_ITEM(2)
}